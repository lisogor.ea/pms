package pms.api.domain

import java.util.*

class ApiError (val time: Date, val message: String, val errors: HashMap<String,ArrayList<String>>) {
}