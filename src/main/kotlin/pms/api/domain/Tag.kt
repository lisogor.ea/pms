package pms.api.domain
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "tags")
data class Tag (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Long?,

    @Column(name = "name", nullable = false)
    @field:NotNull(message = "Name is mandatory")
    @field:NotBlank(message = "Name cannot be empty")
    @field:Size(min = 1, max = 100, message = "Name length should be between 1 and 100")
    var name: String
)