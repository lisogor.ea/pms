package pms.api.domain

import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.persistence.FetchType

@Entity
@Table(name = "tasks")
data class Task (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Long?,

    @Column(name = "name", nullable = false)
    @field:NotNull(message = "Name is mandatory")
    @field:NotBlank(message = "Name cannot be empty")
    @field:Size(min = 1, max = 200, message = "Name length should be between 1 and 200")
    var name: String,

    @Column(name = "description", nullable = true)
    var description: String,

    @Column(name = "createddate", nullable = true)
    var createdDate: Date,

    @ManyToOne(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.REFRESH))
    @JoinColumn(name = "tagid", nullable = true)
    var tag: Tag?
)