package pms.api.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pms.api.domain.Tag
import pms.api.service.TagService
import javax.validation.Valid

@RestController
class TagController {

    @Autowired
    private val tagService: TagService? = null

    @RequestMapping("/tags", method = arrayOf(RequestMethod.GET))
    fun getTags() = tagService?.getTags();

    @RequestMapping("/tag", method = arrayOf(RequestMethod.POST))
    fun updateTag(@Valid @RequestBody tag: Tag): ResponseEntity<Any> {
        tagService?.saveTag(tag);
        return ResponseEntity.ok(tag);
    }

    @RequestMapping("/tag", method = arrayOf(RequestMethod.DELETE))
    fun deleteTag(@RequestParam(value = "id") id: Long): ResponseEntity<Any> {
        tagService?.deleteTag(id);
        return ResponseEntity.ok(null);
    }
}