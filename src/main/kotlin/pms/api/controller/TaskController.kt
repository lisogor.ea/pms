package pms.api.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pms.api.domain.Tag
import pms.api.domain.Task
import pms.api.service.TaskService
import java.util.*
import javax.validation.Valid

@RestController
class TaskController {

    @Autowired
    private val taskService: TaskService? = null

    @RequestMapping("/tasks", method = arrayOf(RequestMethod.GET))
    fun getTasks() = taskService?.getTasks();

    @RequestMapping("/task", method = arrayOf(RequestMethod.POST))
    fun updateTask(@Valid @RequestBody task: Task): ResponseEntity<Any> {

        if (task.tag is Tag && task.tag!!.id == null) {
            task.tag = null;
        }

        taskService?.saveTask(task);
        return ResponseEntity.ok(task);
    }

    @RequestMapping("/task", method = arrayOf(RequestMethod.DELETE))
    fun deleteTask(@RequestParam(value = "id") id: Long) {
        taskService?.deleteTask(id);
    }
}