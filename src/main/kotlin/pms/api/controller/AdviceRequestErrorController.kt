package pms.api.controller

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.NoHandlerFoundException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*
import java.util.HashMap
import org.springframework.web.bind.MethodArgumentNotValidException
import pms.api.domain.ApiError
import kotlin.collections.ArrayList


@ControllerAdvice
class AdviceRequestErrorController : ResponseEntityExceptionHandler() {

    /*
    override fun handleNoHandlerFoundException(ex: NoHandlerFoundException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {

        val errorDetails = ErrorsDetails(
            Date(),
            "Validation Failed",
            ex.message!!
        )

        print("HERE");

        // return super.handleExceptionInternal(ex, null, headers, status, request)
        // return response(ex, request, HttpStatus.BAD_REQUEST, "Validation Failed");
        // return ResponseEntity.handleExceptionInternal(ex, message, header(), status, request);
        return ResponseEntity(errorDetails, org.springframework.http.HttpStatus.BAD_REQUEST)
    }
    */

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {

        var errors:HashMap<String, ArrayList<String>> = HashMap<String, ArrayList<String>>();

        ex.getBindingResult().getAllErrors().forEach({
            var fieldName: String = it.objectName;
            var errorMessage: String = it.defaultMessage;
            System.out.println(fieldName);
            System.out.println(errorMessage);
            System.out.println("--------------------------------------------");
            if (!errors.containsKey(fieldName)) {
                errors.put(fieldName, ArrayList<String>());
            }
            errors.get(fieldName)?.add(errorMessage);
        });

        val errorDetails = ApiError(
            Date(),
            "Validation Failed",
            errors
        )

        return ResponseEntity(errorDetails, headers, org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY);
    }

            /*
    @ExceptionHandler(value = [(NoHandlerFoundException::class )])
    fun notFound(ex: NoHandlerFoundException,request: WebRequest): ResponseEntity<ErrorsDetails> {
        val errorDetails = ErrorsDetails(
            Date(),
            "Not found",
            ex.message!!
        )
        return ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST)
    }*/

}