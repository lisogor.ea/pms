package pms.api.exception

class TaskDoesNotExistException() : Exception("Task doesn't exist")