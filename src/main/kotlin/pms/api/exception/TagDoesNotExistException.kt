package pms.api.exception

class TagDoesNotExistException() : Exception("Tag doesn't exist")