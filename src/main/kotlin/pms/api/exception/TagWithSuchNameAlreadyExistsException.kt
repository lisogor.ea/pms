package pms.api.exception

class TagWithSuchNameAlreadyExistsException() : Exception("Tag with such name already exists")