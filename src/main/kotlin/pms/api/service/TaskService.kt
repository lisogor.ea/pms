package pms.api.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pms.api.domain.Task
import pms.api.exception.TagDoesNotExistException
import pms.api.exception.TaskDoesNotExistException
import pms.api.repository.TagRepository
import pms.api.repository.TaskRepository
import java.util.*

@Service
class TaskService(
    @field:Autowired private val taskRepository: TaskRepository,
    @field:Autowired private val tagRepository: TagRepository) {

    fun getTasks(): List<Task> {
        return taskRepository.findAll();
    }

    @Transactional
    @Throws(TaskDoesNotExistException::class, TagDoesNotExistException::class)
    fun saveTask(task: Task): Task {
        // Идентификатор объекта есть => проверяем существует ли объект с таким ID
        if ((task.id is Long) && !taskRepository.existsById(task.id)) {
            throw TaskDoesNotExistException();
        } else {
            task.createdDate = Date();
        }

        if (task.tag != null) {
            task.tag = tagRepository.findById(task.tag!!.id).orElseThrow{TagDoesNotExistException()};
        }

        taskRepository.save(task);

        return task;
    }

    @Transactional
    @Throws(TaskDoesNotExistException::class)
    fun deleteTask(id: Long) {
        var task: Task = taskRepository.findById(id).orElseThrow{TaskDoesNotExistException()};

        taskRepository.delete(task);
    }

}