package pms.api.service

import pms.api.domain.Tag
import pms.api.repository.TagRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pms.api.exception.TagDoesNotExistException
import pms.api.exception.TagWithSuchNameAlreadyExistsException
import pms.api.repository.TaskRepository

@Service
class TagService(
    @field:Autowired private val tagRepository: TagRepository,
    @field:Autowired private val taskRepository: TaskRepository) {

    fun getTags(): List<Tag> {
        return tagRepository.findAll();
    }

    @Throws(TagDoesNotExistException::class, TagWithSuchNameAlreadyExistsException::class)
    fun saveTag(tag: Tag): Tag {
        // Идентификатор объекта есть => проверяем существует ли объект с таким ID
        if ((tag.id is Long) && !tagRepository.existsById(tag.id)) {
            throw TagDoesNotExistException();
        }

        try {
            tagRepository.save(tag);
        } catch (e: DataIntegrityViolationException) {
            throw TagWithSuchNameAlreadyExistsException();
        }

        return tag;
    }

    @Transactional
    @Throws(TagDoesNotExistException::class)
    fun deleteTag(id: Long) {
        var tag: Tag = tagRepository.findById(id).orElseThrow{TagDoesNotExistException()};

        // Delete related entities
        taskRepository.deleteAllByTagId(tag.id!!);
        tagRepository.delete(tag);
    }
}
