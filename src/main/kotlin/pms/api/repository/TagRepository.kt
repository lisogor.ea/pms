package pms.api.repository

import pms.api.domain.Tag
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository


@Repository
interface TagRepository : CrudRepository<Tag, Long> {
    override fun findAll(): List<Tag>
}
