package pms.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import pms.api.domain.Task

interface TaskRepository : JpaRepository<Task, Long> {

    override fun findAll(): List<Task>

    @Modifying
    @Query(value = "delete from tasks t where t.tagid=:tagid", nativeQuery = true)
    fun deleteAllByTagId(@Param("tagid") tagId: Long)
}
