-- Table: pms.tags

-- DROP TABLE pms.tags;

CREATE TABLE pms.tags
(
    id integer NOT NULL DEFAULT nextval('pms.tags_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tags_pk PRIMARY KEY (id),
    CONSTRAINT tags_name_uk UNIQUE (name)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE pms.tags
    OWNER to postgres;


-- Table: pms.tasks

-- DROP TABLE pms.tasks;

CREATE TABLE pms.tasks
(
    id integer NOT NULL DEFAULT nextval('pms.tasks_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    createddate timestamp without time zone NOT NULL,
    tagid integer,
    updateddate timestamp without time zone,
    CONSTRAINT tasks_pk PRIMARY KEY (id),
    CONSTRAINT tasks_tagid_fk FOREIGN KEY (tagid)
        REFERENCES pms.tags (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE pms.tasks
    OWNER to postgres;

-- Index: fki_tasks_tagid_fk

-- DROP INDEX pms.fki_tasks_tagid_fk;

CREATE INDEX fki_tasks_tagid_fk
    ON pms.tasks USING btree
    (tagid)
    TABLESPACE pg_default;